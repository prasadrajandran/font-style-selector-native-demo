export default class OptionButton extends HTMLButtonElement {

  constructor() {
    super();

    this.addEventListener('click', this.toggle);
  }

  public get selected(): boolean {
    return this.hasAttribute('selected');
  }

  public set selected(val: boolean) {
    if (val) {
      this.setAttribute('selected', '');
    } else {
      this.removeAttribute('selected');
    }
  }

  public toggle(): void {
    this.selected = !this.selected;
  }
}

export function registerOptionButtonComponent(name: string = 'pr-btn-opt'): void {
  window.customElements.define(name, OptionButton, { extends: 'button' });
}

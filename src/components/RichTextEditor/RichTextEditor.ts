const HTML_TEMPLATE = `
  <style>${require('./RichTextEditor.css')}</style>
  ${require('./RichTextEditor.html')}
`;

import ButtonGroup from '../ButtonGroup/ButtonGroup';
import OptionButton from '../OptionButton/OptionButton';

export default class RichTextEditor extends HTMLElement {

  private template: HTMLTemplateElement = document.createElement('template');
  private editor: HTMLDivElement = document.createElement('div');
  private observer: MutationObserver;
  private styleButtons: ButtonGroup = new ButtonGroup();

  constructor() {
    super();

    this.initTemplate();
    
    this.observer = new MutationObserver(() => this.syncStyles('editor'));
    this.observer.observe(this.styleButtons, {
      attributeFilter: ['selected'],
      attributes: true
    });
  }

  public get rteStyles(): string[] {
    if (this.hasAttribute('rte-styles')) {
      return this.getAttribute('rte-styles').split(',');
    }
    return [];
  }

  public set rteStyles(val: string[]) {
    this.setAttribute('rte-styles', val.toString());
  }

  public get rteLabels(): string[] {
    if (this.hasAttribute('rte-labels')) {
      return this.getAttribute('rte-labels').split(',');
    }
    return [];
  }

  public set rteLabels(val: string[]) {
    this.setAttribute('rte-labels', val.toString());
  }

  private initTemplate(): void {
    this.template.innerHTML = HTML_TEMPLATE;

    this.rteStyles.forEach((style: string, i: number) => {
      let buttonOption = new OptionButton();

      buttonOption.value = style;
      buttonOption.innerHTML = this.rteLabels[i];

      this.styleButtons.appendChild(buttonOption);
    });
    this.template.content.appendChild(this.styleButtons);

    this.editor.contentEditable = 'true';
    this.editor.addEventListener(
      'keyup',
      this.syncStyles.bind(this, 'buttons')
    );
    this.template.content.appendChild(this.editor);

    this.attachShadow({ mode: 'open' }).appendChild(this.template.content);
  }

  private syncStyles(direction: 'editor' | 'buttons'): void {
    this.editor.focus();

    this.styleButtons.buttons.forEach((optBtn: OptionButton) => {
      let styleActive: boolean = document.queryCommandState(optBtn.value);

      if (styleActive === optBtn.selected) {
        return;
      }

      if (direction === 'editor') {
        document.execCommand(optBtn.value, false);
      } else if (direction === 'buttons') {
        optBtn.click();
      }
    });
  }
}

export function registerRichTextEditorComponent(name: string = 'pr-editor') {
  window.customElements.define(name, RichTextEditor);
}

const HTML_TEMPLATE = `
  <style>${require('./Demo.css')}</style>
  ${require('./Demo.html')}
`;

import RichTextEditor from '../RichTextEditor/RichTextEditor';

export default class Demo extends HTMLElement {

  private demos: RTEDemo[] = [
    {
      styles: ['bold', 'italic', 'underline'],
      labels: [
        '<span style="font-weight: bold">Bold</span>',
        '<span style="font-style: italic">Italic</span>',
        '<span style="text-decoration: underline">Underline</span>'
      ]
    },
    {
      styles: ['strikeThrough', 'subscript'],
      labels: [
        '<span style="text-decoration: line-through">Strikethrough</span>',
        'T<sub>x</sub>'
      ]
    },
    {
      styles: ['bold'],
      labels: [
        '<span style="font-weight: bold">Bold</span>',
      ]
    }
  ];

  private template: HTMLTemplateElement = document.createElement('template');

  constructor() {
    super();

    this.initTemplate();
  }

  private initTemplate(): void {
    this.template.innerHTML = HTML_TEMPLATE;

    this.demos.forEach((demo: RTEDemo, i: number) => {
      let heading = document.createElement('h2');
      heading.innerHTML = 'Example ' + (i + 1);
      this.template.content.appendChild(heading);

      let rte = new RichTextEditor();
      rte.rteStyles = demo.styles;
      rte.rteLabels = demo.labels;
      this.template.content.appendChild(rte);

      if (i !== this.demos.length - 1) {
        this.template.content.appendChild(document.createElement('hr'));
      }
    });

    this.attachShadow({ mode: 'open' })
      .appendChild(this.template.content.cloneNode(true));
  }
}

class RTEDemo {

  styles: string[];
  labels: string[];
}

export function registerDemoComponent(name: string = 'pr-demo') {
  window.customElements.define(name, Demo);
}

const HTML_TEMPLATE = `
  <style>${require('./ButtonGroup.css')}</style>
  ${require('./ButtonGroup.html')}
`;

import OptionButton from '../OptionButton/OptionButton';

export default class ButtonGroup extends HTMLElement {

  private template: HTMLTemplateElement = document.createElement('template');
  private observer: MutationObserver;
  public buttons: OptionButton[] = [];

  constructor() {
    super();
    
    this.initTemplate();
    this.initButtons();
    this.syncSelectedAttribute();
    this.addEventListener('click', this.syncSelectedAttribute);

    // Update list of buttons every time a new child node is inserted
    this.observer = new MutationObserver(() => this.initButtons());
    this.observer.observe(this, { childList: true });
  }

  public set selected(val: string[]) {
    val = Array.isArray(val) ? val : [val];
    this.setAttribute('selected', val.toString());
    this.buttons.forEach(
      (btn: OptionButton) => btn.selected = val.indexOf(btn.value) >= 0
    );
  }

  public get selected(): string[] {
    if (this.hasAttribute('selected')) {
      return this.getAttribute('selected').split(',');
    }
    return [];
  }

  private initTemplate(): void {
    this.template.innerHTML = HTML_TEMPLATE;
    this.attachShadow({ mode: 'open' })
      .appendChild(this.template.content.cloneNode(true));
  }

  private initButtons(): void {
    for (let i = 0; i < this.children.length; i++) {
      let btn = this.children.item(i);
      if (btn.nodeName === 'BUTTON') {
        this.buttons.push(btn as OptionButton);
      }
    }
  }

  private syncSelectedAttribute(): void {
    this.setAttribute(
      'selected',
      this.buttons
        .filter((btn: OptionButton) => btn.selected)
        .map((btn: OptionButton) => btn.value)
        .toString()
    );
  }
}

export function registerButtonGroupComponent(name: string = 'pr-btn-grp'): void {
  window.customElements.define(name, ButtonGroup);
}

import { registerButtonGroupComponent }from './components/ButtonGroup/ButtonGroup';
import { registerOptionButtonComponent } from './components/OptionButton/OptionButton';
import { registerRichTextEditorComponent } from './components/RichTextEditor/RichTextEditor';
import { registerDemoComponent } from './components/Demo/Demo';

(() => {
  registerButtonGroupComponent();
  registerOptionButtonComponent();
  registerRichTextEditorComponent();
  registerDemoComponent();
})();

# Font Style Selected Native Demo  

## Introduction  

Built using custom elements, the shadow DOM, and TypeScript. Probably only works
on the very latest version (67) of Chrome.  

## Demo  

* [https://demo.prasadpr.com/native/](https://demo.prasadpr.com/native/)  

## Components  

* [ButtonGroup](https://bitbucket.org/prasadrajandran/font-style-selector-native-demo/src/master/src/components/ButtonGroup/)  
* [OptionButton](https://bitbucket.org/prasadrajandran/font-style-selector-native-demo/src/master/src/components/OptionButton/OptionButton.ts)  

## Other Components  

* [RichTextEditor](https://bitbucket.org/prasadrajandran/font-style-selector-native-demo/src/master/src/components/RichTextEditor/)  
* [Demo](https://bitbucket.org/prasadrajandran/font-style-selector-native-demo/src/master/src/components/Demo/)  

## Author  

Prasad Rajandran (prasadpr@outlook.com)  
